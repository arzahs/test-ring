# coding: utf-8
import collections

def check_diagonal_on_same_number(matrix):
    dd = collections.defaultdict(list)
    for i in range(len(matrix) - 1, -1, -1):
        for j in range(0, len(matrix[i])):
            dd[i-j].append(matrix[i][j])

    for items in dd.values():
        if len(set(items)) > 1:
            return False
    return True

matrix = input("matrix = ")
result = check_diagonal_on_same_number(matrix)
print result