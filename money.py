# coding: utf-8
def get_money(money_sum):
    ukrainian_coins = [200, 100, 50, 25, 10, 5, 2, 1]
    ways = [0] * (money_sum + 1)
    ways[0] = 1

    for i in range(len(ukrainian_coins)):
        for j in range(ukrainian_coins[i], money_sum + 1):
            ways[j] += ways[j - ukrainian_coins[i]]
    
    result = ways[money_sum]
    return result

value = float(input("Enter value: ")) 
print get_money(int(value * 100))