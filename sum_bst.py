# coding: utf-8
from collections import deque

class Node:
    def __init__(self, data, left=None, right=None):
        self.data = data
        self.left = left
        self.right = right

    def insert(self, node):
        if self.data < node.data:
            if self.left is None:
                self.left = node
            else:
                self.left.insert(node)
        if self.data > node.data:
            if self.right is None:
                self.right = node
            else:
                self.right.insert(node)


def find_sum(root, target):
    search = set()
    queue = deque((root.left, root.right))

    while True:
        try:
            node = queue.popleft()
        except IndexError:
            return False
        if node is None:
            continue
        y = target - node.data
        if y in search:
            return True
        queue.extend((node.left, node.right))
        search.add(node.data)

# tree mockup
root_tree = Node(8, None, None)
root_tree.insert(Node(3, None, None))
root_tree.insert(Node(4, None, None))
root_tree.insert(Node(6, Node(4), Node(7)))

target = float(input("Enter value: ")) 
print(find_sum(root_tree, target))
